import java.util.Scanner;
public class FactorialNum {
	public static void main(String[] args) {
		Scanner teclado= new Scanner (System.in);
		float numero, fact;
		System.out.println("Ingrese el el numero al que desea calcular su factorial");
        numero= teclado.nextInt();
        fact=1;
        if (numero>0) {
            for (int i =1; i <=numero; i++) {
               fact=fact*i;
            }
            System.out.println("el factorial es " +fact);
        } else {
            System.out.println("el factorial de un numero negativo no existe");
        }
        
        

    }
}
import java.util.Scanner;
public class ValorPI {
    public static void main(String[] args) throws Exception {
        int x = 1;
        double PI = 0;
        Scanner entrada = new Scanner(System.in);

        System.out.println("Numero de terminos");
        x = entrada.nextInt();

        for (int i=1 ; i<=x; i++){
             PI = PI + ((2*i)/(2*i-1)) * ((2*i) / (2*i +1));
        }
        System.out.println("PI = "+PI);
    }
}

import java.util.Scanner;
//1) Determinar el descuento proporcionado por una tienda a sus clientes. Se debe
//  solicitar el monto total de las compras y aplicar le descuento según la tabla, al
//  final se debe mostrar cual es la categoría del descuento, y su total a pagar
//  (quitando el descuento).
public class CompraDescuento {
    public static void main(String[] args){
        double desc=0;
        double compra=0;
        double Total=0;
        Scanner teclado = new Scanner (System.in);

        System.out.println("Introduzca el valor de su compra");
        compra = teclado.nextDouble();

        if (compra<450){
        desc=0;
        }else if ((compra>450) & (compra<=800)) {
        desc=0.10;
        }else if (compra>800){
            desc=0.20;
        }

        Total = ((compra) - (compra*desc));

        System.out.println("");

        System.out.println("Sub-Total: "+compra);
        System.out.print("Descuento: "+(desc*100));
        System.out.println(" %");
        System.out.println("Total a pagar "+Total);
    }
}